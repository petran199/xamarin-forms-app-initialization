# xamarin-forms-app-initialization

### The project aims to help xamarin forms devs starting with an all in one template.
### There are two opptions that somebody can choose as a starting point of template.

1. Using Tabbed page architecture.

2. Using Master Detail page architecture.

---

# Currently there is only support for Mac users

## Usage:

1. Clone and navigate to the project.

2. Create a Blank Forms App from visual studio for mac.

3. run the script giving the path of the blank forms app as a parmeter.

    $ ./bootstrap.sh absolute_path_of_blank_forms_app

    e.g. 

    `$ ./bootstrap.sh /Users/myUserName/MyBlankApp/`

4. Wait till the process has finished and navigate to the visual studio and when there is a propt to reload, just hit reload.

---

## Errors:

#### In rare cases there might be an error after reload. In that case:

1. Close the app solution from visual studio.

2. Reopen it, and make a fresh rebuild.

3. If the above don't help, try closing the visual studio application and reopen that, then try again the steps 1. and 2.
