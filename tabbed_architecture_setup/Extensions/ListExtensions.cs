﻿using System.Collections.Generic;
using MvvmHelpers;

namespace MyApp.Extensions
{
    public static class ListExtensions
    {
        public static ObservableRangeCollection<T> ToObservableRangeCollection<T>(this IEnumerable<T> list)
        {
            var collection = new ObservableRangeCollection<T>();
            collection.AddRange(list);

            return collection;
        }
    }
}

