﻿using System;
using System.Threading.Tasks;
using MyApp.Bootstrap;
using MyApp.Contracts.Services.General;
using MyApp.ResX;
using Xamarin.Forms;

namespace MyApp
{
    public partial class App : Application
    {
        private readonly INavigationService _navigationService;
        private readonly ISettingsService _settingsService;

        public App()
        {
            InitializeComponent();

            var dsLocalization = DependencyService.Get<ILocalize>();
            var ci = dsLocalization.GetCurrentCultureInfo();

            AppResources.Culture = ci; // set the RESX for resource localization
            dsLocalization.SetLocale(ci); // set the Thread for locale-aware methods

            InitializeApp();

            _navigationService = AppContainer.Resolve<INavigationService>();
            _settingsService = AppContainer.Resolve<ISettingsService>();
            _settingsService.AppVersion = DependencyService.Get<IApplication>().GetAppVersion();

            InitializeNavigation();
        }

        private async Task RefreshTimelineAsync()
        {
            if (_settingsService.LastAppOpened < DateTime.Now.AddMinutes(-2))
            {
                await _navigationService.InitializeAsync();
            }
        }

        private void InitializeApp()
        {
            AppContainer.RegisterDependencies();
        }

        private async void InitializeNavigation()
        {
            await _navigationService.InitializeAsync();
        }

        protected override void OnStart()
        {
            _settingsService.ApplicationSleeps = false;

        }

        protected override void OnResume()
        {
            _settingsService.ApplicationSleeps = false;
        }

        protected override void OnSleep()
        {
            base.OnSleep();

            _settingsService.ApplicationSleeps = true;
        }
        
    }
}
