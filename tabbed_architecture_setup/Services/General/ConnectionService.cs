﻿using System;
using System.Threading.Tasks;
using MyApp.Contracts.Repository;
using MyApp.Contracts.Services.General;
using MyApp.Helpers;
using MyApp.ResX;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Xamarin.Forms;

namespace MyApp.Services.General
{
    public class ConnectionService : IConnectionService
    {
        private readonly IConnectivity _connectivity;
        private readonly IGenericRepository _genericRepository;
        private readonly ISettingsService _settingsService;

        public ConnectionService(IGenericRepository genericRepository,
                                 ISettingsService settingsService)
        {
            _genericRepository = genericRepository;
            _settingsService = settingsService;

            _connectivity = CrossConnectivity.Current;
            _connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
        }

        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            ConnectivityChanged?.Invoke(this, new ConnectivityChangedEventArgs { IsConnected = e.IsConnected });
        }

        public event ConnectivityChangedEventHandler ConnectivityChanged;

        public async Task<bool> DoIHaveInternet(bool silentCheck = false)
        {
            if (!_connectivity.IsConnected && !silentCheck)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Error no internet connection", "OK");
            }

            return _connectivity.IsConnected;
        }

        public async Task<bool> CheckIfUpdateIsAvailable()
        {
            if (await DoIHaveInternet())
            {
                //TODO Uncomment when you have implemented the REST call
                //var latestVersion = new Version(await _genericRepository.GetAsync<string>(Constants.REST_API_GET_LATEST_RELEASE_VERSION, _settingsService.AuthToken));
                var appVersion = new Version(_settingsService.AppVersion);
                var latestVersion = appVersion;

                if (latestVersion.CompareTo(appVersion) > 0)
                {

                    if (await Application.Current.MainPage.DisplayAlert("Info", "The new version of the app is available. Please download it in order to avoid any compatibility issues", "OK", "Cancel"))
                    {
                        Device.OpenUri(new Uri(Constants.DOWNLOAD_APP_URL));

                        DependencyService.Get<IApplication>().CloseApplication();
                    }

                    return true;
                }

                _settingsService.LastUpdateCheck = DateTime.Now;
            }

            return false;
        }
    }
}
