﻿using System;
using MyApp.Contracts.Services.General;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace MyApp.Services.General
{
    public class SettingsService : ISettingsService
    {
        private ISettings AppSettings => CrossSettings.Current;

        public string AppVersion
        {
            get => AppSettings.GetValueOrDefault(nameof(AppVersion), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(AppVersion), value);
        }

        public string AuthToken
        {
            get => AppSettings.GetValueOrDefault(nameof(AuthToken), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(AuthToken), value);
        }

        public DateTime LastLogin
        {
            get => AppSettings.GetValueOrDefault(nameof(LastLogin), DateTime.Now.AddDays(-1));
            set => AppSettings.AddOrUpdateValue(nameof(LastLogin), value);
        }

        public DateTime LastAppOpened
        {
            get => AppSettings.GetValueOrDefault(nameof(LastAppOpened), DateTime.Now);
            set => AppSettings.AddOrUpdateValue(nameof(LastAppOpened), value);
        }

        public DateTime LastUpdateCheck
        {
            get => AppSettings.GetValueOrDefault(nameof(LastUpdateCheck), new DateTime());
            set => AppSettings.AddOrUpdateValue(nameof(LastUpdateCheck), value);
        }

        public bool IsUserLoggedIn
        {
            get => AppSettings.GetValueOrDefault(nameof(IsUserLoggedIn), true);
            set => AppSettings.AddOrUpdateValue(nameof(IsUserLoggedIn), value);
        }

        public string LastLoggedUsername
        {
            get => AppSettings.GetValueOrDefault(nameof(LastLoggedUsername), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(LastLoggedUsername), value);
        }

        public int LastLoggedId
        {
            get => AppSettings.GetValueOrDefault(nameof(LastLoggedId), default(int));
            set => AppSettings.AddOrUpdateValue(nameof(LastLoggedId), value);
        }

        public bool ApplicationSleeps
        {
            get => AppSettings.GetValueOrDefault(nameof(ApplicationSleeps), default(bool));
            set => AppSettings.AddOrUpdateValue(nameof(ApplicationSleeps), value);
        }

    }
}
