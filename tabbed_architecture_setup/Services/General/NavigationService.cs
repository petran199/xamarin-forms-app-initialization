﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using MyApp.Contracts.Services.General;
using MyApp.ViewModels;
using MyApp.Views;
using System.Diagnostics;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using MyApp.Enumerations;
using Rg.Plugins.Popup.Services;

namespace MyApp.Services.General
{
    public class NavigationService : INavigationService
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingsService _settingsService;
        private readonly Dictionary<Type, Type> _mappings;

        protected Application CurrentApplication => Application.Current;

        public NavigationService(IAuthenticationService authenticationService, ISettingsService settingsService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingsService;
            _mappings = new Dictionary<Type, Type>();

            CreatePageViewModelMappings();
        }

        public async Task InitializeAsync()
        {
            if (_authenticationService.NeedsToLogin())
            {
                await NavigateToAsync<LoginViewModel>();
            }
            else
            {
                await NavigateToAsync<MainViewModel>();
            }
        }

        public async Task NavigateBackAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.CurrentPage.Navigation.PopAsync();
            }
        }

        public async Task PopToRootAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.CurrentPage.Navigation.PopToRootAsync();
            }
        }

        public async Task PopBackAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.CurrentPage.Navigation.PopModalAsync();
            }
        }

        public async Task PopPopupAsync()
        {
            await PopupNavigation.Instance.PopAsync();
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        public Task NavigateToAsync(Type viewModelType)
        {
            return InternalNavigateToAsync(viewModelType, null);
        }

        public Task NavigateToAsync(Type viewModelType, object parameter)
        {
            return InternalNavigateToAsync(viewModelType, parameter);
        }

        protected virtual async Task InternalNavigateToAsync(Type viewModelType, object parameter)
        {
            var page = CreatePage(viewModelType, parameter);
            var enumNavigationType = GetNavigationType(page);

            await NavigateToProperType(enumNavigationType, page);

            await (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
        }

        protected Type GetPageTypeForViewModel(Type viewModelType)
        {
            if (!_mappings.ContainsKey(viewModelType))
            {
                throw new KeyNotFoundException($"No map for ${viewModelType} was found on navigation mappings");
            }

            return _mappings[viewModelType];
        }

        protected Page CreatePage(Type viewModelType, object parameter)
        {
            try
            {
                Type pageType = GetPageTypeForViewModel(viewModelType);

                if (pageType == null)
                {
                    throw new Exception($"Mapping type for {viewModelType} is not a page");
                }

                Page page = Activator.CreateInstance(pageType) as Page;

                return page;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        private EnumNavigationType GetNavigationType(Page page)
        {
            switch (page)
            {
                case MainView _:
                case LoginView _:
                    return EnumNavigationType.MainPage;

                case HomeView _:
                    return EnumNavigationType.HomePage;

                default:
                    return EnumNavigationType.SimpleNavigationPage;
            }
        }

        private async Task NavigateToProperType(EnumNavigationType enumNavigationType, Page page)
        {
            if (enumNavigationType == EnumNavigationType.MainPage)
            {
                CurrentApplication.MainPage = page;
            }
            else
            {
                //TODO Uncomment when you are about to use Modal pages or popup pages (from Rg.popup package) 
                //if (enumNavigationType == EnumNavigationType.PopupPage)
                //{
                //    await CurrentApplication.MainPage.Navigation.PushPopupAsync((PopupPage)page);
                //}
                //else
                //{
                    var mainPage = CurrentApplication.MainPage as MainView;
                    var currentPage = mainPage.CurrentPage;

                //    if (enumNavigationType == EnumNavigationType.ModalPage)
                //    {
                //        await currentPage.Navigation.PushModalAsync(new NavigationPage(page));
                //    }
                //    else
                //    {
                        if (enumNavigationType == EnumNavigationType.HomePage)
                        {
                            mainPage.SelectedItem = mainPage.Children[0];
                        }
                        else
                        {
                            await currentPage.Navigation.PushAsync(page);
                        }
                //    }
                //}
            }
        }

        private void CreatePageViewModelMappings()
        {
            _mappings.Add(typeof(LoginViewModel), typeof(LoginView));
            _mappings.Add(typeof(MainViewModel), typeof(MainView));
            _mappings.Add(typeof(HomeViewModel), typeof(HomeView));
        }
    }
}
