﻿using System;
using System.IO;
using System.Threading.Tasks;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using System.Reflection;

namespace MyApp.Helpers
{
    public static class StaticFunctions
    {
        #region Generall

        public static string StreamReaderToString(string documentName)
        {
            try
            {
                string documentContent = "";
                var assembly = IntrospectionExtensions.GetTypeInfo(typeof(StaticFunctions)).Assembly;

                if (string.IsNullOrEmpty(documentName))
                    return documentContent;

                Stream stream = assembly.GetManifestResourceStream(documentName);

                if (stream == null)
                    return documentContent;

                using (var reader = new StreamReader(stream))
                {
                    documentContent = reader.ReadToEnd();
                }

                return documentContent;
            }
            catch (Exception)
            {
                return documentName;
            }

        }

        public static async Task<PermissionStatus> CheckPermissions(Permission permission)
        {
            var permissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
            bool request = false;
            if (permissionStatus == PermissionStatus.Denied)
            {
                if (Device.RuntimePlatform == Device.iOS)
                {

                    var title = $"{permission} Permission";
                    var question = $"To use this plugin the {permission} permission is required. Please go into Settings and turn on {permission} for the app.";
                    var positive = "Settings";
                    var negative = "Maybe Later";
                    var task = Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);
                    if (task == null)
                        return permissionStatus;

                    var result = await task;
                    if (result)
                    {
                        CrossPermissions.Current.OpenAppSettings();
                    }

                    return permissionStatus;
                }

                request = true;

            }

            if (request || permissionStatus != PermissionStatus.Granted)
            {
                var newStatus = await CrossPermissions.Current.RequestPermissionsAsync(permission);

                if (!newStatus.ContainsKey(permission))
                {
                    return permissionStatus;
                }

                permissionStatus = newStatus[permission];

                if (newStatus[permission] != PermissionStatus.Granted)
                {
                    permissionStatus = newStatus[permission];
                    var title = $"{permission} Permission";
                    var question = $"To use the plugin the {permission} permission is required.";
                    var positive = "Settings";
                    var negative = "Maybe Later";
                    var task = Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);
                    if (task == null)
                        return permissionStatus;

                    var result = await task;
                    if (result)
                    {
                        CrossPermissions.Current.OpenAppSettings();
                    }
                    return permissionStatus;
                }
            }

            return permissionStatus;
        }

        public static void SendSupportMail()
        {
            SendEmail(Constants.MAIL_RECEIVER_SUPPORT, Constants.MAIL_SUBJECT_CONTACT_WITH_DEV_TEAM, "");
        }

        #endregion 

        #region Image processing

        public static ImageSource GetImageSourceFromBase64String(string imageString)
        {
            return ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(imageString)));
        }

        #endregion

        #region Email functions

        public static void SendEmail(string receiver, string subject, string body)
        {
            subject = Uri.EscapeUriString(subject);
            body = Uri.EscapeUriString(body);

            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    Device.OpenUri(new Uri($"ms-outlook://compose?to={receiver}&subject={subject}&body={body}"));
                }
                catch (Exception)
                {
                    Application.Current?.MainPage?.DisplayAlert("Warning", "The Outlook app is not installed", "OK");
                }

            });
        }

        #endregion
    }
}
