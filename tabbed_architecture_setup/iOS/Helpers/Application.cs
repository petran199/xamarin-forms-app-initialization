﻿using System.Threading;
using Foundation;
using Xamarin.Forms;
using MyApp.Contracts.Services.General;

[assembly: Dependency(typeof(MyApp.iOS.Helpers.Application))]
namespace MyApp.iOS.Helpers
{
    public class Application : IApplication
    {
        public string GetAppVersion()
        {
            return NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"].ToString();
        }

        public void CloseApplication()
        {
            Thread.CurrentThread.Abort();
        }
    }
}
