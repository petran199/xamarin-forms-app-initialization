﻿using System;

namespace MyApp.Contracts.Services.General
{
    public interface ISettingsService
    {
        string AuthToken { get; set; }
        string AppVersion { get; set; }
        bool ApplicationSleeps { get; set; }
        DateTime LastLogin { get; set; }
        DateTime LastAppOpened { get; set; }
        DateTime LastUpdateCheck { get; set; }
        bool IsUserLoggedIn { get; set; }
        string LastLoggedUsername { get; set; }
        int LastLoggedId { get; set; }
    }
}
