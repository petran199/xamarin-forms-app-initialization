﻿namespace MyApp.Contracts.Services.General
{
    public interface IApplication
    {
        string GetAppVersion();

        void CloseApplication();
    }
}
