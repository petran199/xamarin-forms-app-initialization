﻿using System.Threading.Tasks;
using MyApp.Models;

namespace MyApp.Contracts.Services.General
{
    public interface IAuthenticationService
    {
        bool NeedsToLogin();

        Task<AuthenticationResponse> Authenticate(string userName, string password);
    }
}
