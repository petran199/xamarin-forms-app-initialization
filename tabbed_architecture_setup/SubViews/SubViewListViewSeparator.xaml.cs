﻿using Xamarin.Forms;

namespace MyApp.SubViews
{
    public partial class SubViewListViewSeparator : ContentView
    {
        public SubViewListViewSeparator()
        {
            InitializeComponent();

            CreateSeparatorLine();
        }

        private void CreateSeparatorLine()
        {
            for (int i = 0; i < 40; i++)
            {
                parentGrid.Children.Add(new BoxView(), i, 0);
            }
        }
    }
}
