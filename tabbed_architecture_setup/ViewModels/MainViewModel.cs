﻿using System.Threading.Tasks;
using MyApp.Contracts.Services.General;

namespace MyApp.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private HomeViewModel _homeViewModel;

        public MainViewModel(IConnectionService connectionService,
                             INavigationService navigationService,
                             IDialogService dialogService,
                             HomeViewModel homeViewModel)
            : base(connectionService, navigationService, dialogService)
        {
            _homeViewModel = homeViewModel;
        }

        public HomeViewModel HomeViewModel
        {
            get => _homeViewModel;
            set
            {
                _homeViewModel = value;
                OnPropertyChanged();
            }
        }

        public override async Task InitializeAsync(object data)
        {
            await Task.WhenAll
            (
                _homeViewModel.InitializeAsync(data)
            );
        }
    }
}
