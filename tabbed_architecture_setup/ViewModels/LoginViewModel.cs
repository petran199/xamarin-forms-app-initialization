﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using MyApp.Contracts.Services.General;
using Xamarin.Forms;
using MyApp.Services.General;
using MyApp.Models;

namespace MyApp.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingsService _settingsService;

        private string _username;
        private string _password;


        public ICommand OnLoginCommand => new Command(OnLogin);

        public string Username
        {
            get => _username;
            set
            {
                _username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public LoginViewModel(IConnectionService connectionService,
                              ISettingsService settingsService,
                              INavigationService navigationService,
                              IAuthenticationService authenticationService,
                              IDialogService dialogService)
            : base(connectionService, navigationService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingsService;
        }

        public async override Task InitializeAsync(object data)
        {
            if (!string.IsNullOrEmpty(_settingsService.LastLoggedUsername))
            {
                Username = _settingsService.LastLoggedUsername;
            }

            await Task.FromResult(false);
        }

        /// <summary>
        /// Validates the user input.
        /// </summary>
        private async Task<bool> ValidateUserInput()
        {
            var username = _username.TrimEnd(' ').TrimStart(' ');

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(_password))
            {
                await _dialogService.ShowDialog("Error", "Username or password cann not be empty", "OK");
                return false;
            }

            var regexItem = new Regex("^[a-zA-Z-]*$");

            if (!regexItem.IsMatch(username))
            {
                await _dialogService.ShowDialog("Error", "Username cannot contain special charackters or numbers", "OK");
                return false;
            }

            return true;
        }


        /// <summary>
        /// Handles login request
        /// </summary>
        private async void OnLogin()
        {
            try
            {
                IsBusy = true;

                if (await _connectionService.DoIHaveInternet())
                {
                    if (await ValidateUserInput())
                    {
                        var username = _username.TrimEnd(' ').TrimStart(' ');
                        var password = _password.TrimEnd(' ').TrimStart(' ');

                        //Implement your own authentication and uncomment
                        //var authenticationResponse = await _authenticationService.Authenticate(Username, Password);
                        var authenticationResponse = new AuthenticationResponse
                        {
                            IsAuthenticated = true,
                            User = new UserModel
                            {
                                Id = 1,
                                FirstName = "Some",
                                LastName = "One",
                                Email = "someone@gmail.com"
                            }
                        };

                        if (authenticationResponse.IsAuthenticated)
                        {
                            _settingsService.LastLogin = _settingsService.LastAppOpened = DateTime.Now.AddHours(1);
                            _settingsService.IsUserLoggedIn = true;
                            _settingsService.LastLoggedId = authenticationResponse.User.Id;
                            _settingsService.LastLoggedUsername = Username;
                            Password = "";

                            await _navigationService.NavigateToAsync<MainViewModel>();
                          }
                        else
                        {
                            await _dialogService.ShowDialog("Error", "Username or password is incorrect", "OK");
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
                await _dialogService.ShowDialog("Error", "Permissions denied", "OK");
            }
            catch (Exception )
            {
                //Handle the exception
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
