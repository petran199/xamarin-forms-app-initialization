﻿using System;
using System.Threading;
using Android.Content.PM;
using MyApp.Contracts.Services.General;
using MyApp.Helpers;
using Xamarin.Forms;

[assembly: Dependency(typeof(MyApp.Droid.Helpers.Application))]
namespace MyApp.Droid.Helpers
{

    public class Application :IApplication
    {
        public void CloseApplication()
        {
            Thread.CurrentThread.Abort();
        }

        public string GetAppVersion()
        {
            var context = global::Android.App.Application.Context;

            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionName;
        }
    }
}
