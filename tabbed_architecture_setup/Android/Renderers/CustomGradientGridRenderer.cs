﻿using System;
using MyApp.CustomControls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content;
using Android.Graphics;
using MyApp.Droid.Renderers;

[assembly: ExportRenderer(typeof(CustomGradientGrid), typeof(CustomGradientGridRenderer))]
namespace MyApp.Droid.Renderers
{
    public class CustomGradientGridRenderer : VisualElementRenderer<Grid>
    {
        private Xamarin.Forms.Color StartColor { get; set; }
        private Xamarin.Forms.Color EndColor { get; set; }

        public CustomGradientGridRenderer(Context context) : base(context)
        {
        }

        protected override void DispatchDraw(Canvas canvas)
        {
            var gradient = new LinearGradient(Height, 0, 0, Height,
                StartColor.ToAndroid(),
                EndColor.ToAndroid(),
                Shader.TileMode.Clamp);
            var paint = new Paint()
            {
                Dither = true,
            };
            paint.SetShader(gradient);
            canvas.DrawPaint(paint);
            base.DispatchDraw(canvas);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Grid> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }

            var page = e.NewElement as CustomGradientGrid;
            StartColor = page.StartColor;
            EndColor = page.EndColor;
        }

    }
}
