﻿namespace MyApp.Enumerations
{
    public enum EnumNavigationType
    {
        MainPage,
        HomePage,
        SimpleNavigationPage,
        ModalPage,
        PopupPage
    }
}
