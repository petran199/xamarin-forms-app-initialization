#!/usr/bin/env bash

[ $# -eq 0 ] && { echo "Usage: $0 absolute_path_of_your_app"; exit 1; }

#Ask weather user want the default Tabbed page or the masterdetail page to be added to the proj
user_selected_masterdetail_initialisation="false"

read -r -p "By default the script adds a Tabbed page in the project. Do you want to add Master-detail instead? [Y/n]: " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
   user_selected_masterdetail_initialisation="true"
   echo "Ok, let's create the Master detail architecture..."
else
  echo "Ok, let's create the default Tabbed architectue..."
fi

if [ -d $abs_path_dir ]
then
   #Trim slash at the and of abs_path_dir
   abs_path_dir=$(echo $1 | sed 's:/*$::')
   echo "$abs_path_dir found and setting up content, please wait..."

   #Vars
   app_basename=$(basename -s .sln $(find $abs_path_dir -name *.sln))
   app_ios_dir="$abs_path_dir/$app_basename.iOS"
   app_android_dir="$abs_path_dir/$app_basename.Android"
   app_info_plist=$app_ios_dir"/Info.plist"
   tabbed_arch_dir_path=$(pwd)"/tabbed_architecture_setup"
   tabbed_arch_ios_dir=$tabbed_arch_dir_path"/iOS"
   tabbed_arch_android_dir=$tabbed_arch_dir_path"/Android"
   masterdetail_arch_dir_path=$(pwd)"/masterdetail_architecture_setup"
   masterdetail_arch_ios_dir=$masterdetail_arch_dir_path"/iOS"
   masterdetail_arch_android_dir=$masterdetail_arch_dir_path"/Android"
   tabbed_script_info_plist=$tabbed_arch_dir_path"/iOS/Info.plist"
   masterdetail_script_info_plist=$masterdetail_arch_dir_path"/iOS/Info.plist"
   app_basename_dir_path="$abs_path_dir/$app_basename"
   app_basename_ios_dir_path="$abs_path_dir/$app_basename.iOS"
   app_basename_android_dir_path="$abs_path_dir/$app_basename.Android"
   app_csproj_file="$app_basename_dir_path/$app_basename.csproj"
   app_ios_csproj_file="$app_basename_ios_dir_path/$app_basename.iOS.csproj"
   app_android_csproj_file="$app_basename_android_dir_path/$app_basename.Android.csproj"
   target_app_cf_bundle_identifier=$(grep -A1 "CFBundleIdentifier" $app_info_plist | awk 'NR==2 {print}')
   tabbed_script_cf_bundle_identifier=$(grep -A1 "CFBundleIdentifier" $tabbed_script_info_plist | awk 'NR==2 {print}')
   masterdetail_script_cf_bundle_identifier=$(grep -A1 "CFBundleIdentifier" $masterdetail_script_info_plist | awk 'NR==2 {print}')
   tabbed_arch_ios_csproj_basename=$(ls $tabbed_arch_ios_dir | grep .csproj | awk -F '.' '{print $1}')
   tabbed_arch_android_csproj_basename=$(ls $tabbed_arch_android_dir | grep .csproj | awk -F '.' '{print $1}')
   masterdetailt_arch_ios_csproj_basename=$(ls $masterdetail_arch_ios_dir | grep .csproj | awk -F '.' '{print $1}')
   masterdetail_arch_android_csproj_basename=$(ls $masterdetail_arch_android_dir | grep .csproj | awk -F '.' '{print $1}')


   # if user chose masterdetail content copy the apropriate files in project.
   if [ $user_selected_masterdetail_initialisation == "false" ]
   then
       #1) Update names in local folders to much the name of the app
       #2) Update the local CFBundelIdentifier value in info plist.
       mv  "$tabbed_arch_ios_dir/$tabbed_arch_ios_csproj_basename.iOS.csproj" "$tabbed_arch_ios_dir/$app_basename.iOS.csproj"
       mv  "$tabbed_arch_android_dir/$tabbed_arch_android_csproj_basename.Android.csproj" "$tabbed_arch_android_dir/$app_basename.Android.csproj"
       sudo ./python_scripts/main.py $abs_path_dir $target_app_cf_bundle_identifier $tabbed_script_cf_bundle_identifier $app_basename $tabbed_script_info_plist

       #Copy local directories to app's path
       echo "Coppying local directories to the app path..."
       rm -rf $app_basename_dir_path"/MainPage.*"
       cp -f $tabbed_arch_dir_path"/App.xaml" $tabbed_arch_dir_path"/App.xaml.cs" $app_basename_dir_path
       rsync -av --exclude '.DS*' --exclude 'App.*' --exclude 'iOS' --exclude 'Android' $tabbed_arch_dir_path"/" $app_basename_dir_path
       #Copy local iOS directories to app's path
       echo "Coppying iOS spesific directories to the app path..."
       rsync -av --exclude '.DS*'  $tabbed_arch_ios_dir"/" $app_ios_dir
       #Copy local Android directories to app's path
       echo "Coppying Android spesific directories to the app path..."
       rsync -av --exclude '.DS*' --exclude 'AndroidManifest.xml' $tabbed_arch_android_dir"/" $app_android_dir
       #Copy also the manifest in the correct app directory
       cp -f $tabbed_arch_android_dir"/AndroidManifest.xml" $app_android_dir"/Properties/"
   else
     #1) Update names in local folders to much the name of the app
     #2) Update the local CFBundelIdentifier value in info plist.
     mv  "$masterdetail_arch_ios_dir/$masterdetailt_arch_ios_csproj_basename.iOS.csproj" "$masterdetail_arch_ios_dir/$app_basename.iOS.csproj"
     mv  "$masterdetail_arch_android_dir/$masterdetail_arch_android_csproj_basename.Android.csproj" "$masterdetail_arch_android_dir/$app_basename.Android.csproj"
     sudo ./python_scripts/main.py $abs_path_dir $target_app_cf_bundle_identifier $masterdetail_script_cf_bundle_identifier $app_basename $masterdetail_script_info_plist

     #Copy local directories to app's path
     echo "Coppying local directories to the app path..."
     rm -rf $app_basename_dir_path"/MainPage.*"
     cp -f $masterdetail_arch_dir_path"/App.xaml" $masterdetail_arch_dir_path"/App.xaml.cs" $app_basename_dir_path
     rsync -av --exclude '.DS*' --exclude 'App.*' --exclude 'iOS' --exclude 'Android' $masterdetail_arch_dir_path"/" $app_basename_dir_path
     #Copy local iOS directories to app's path
     echo "Coppying iOS spesific directories to the app path..."
     rsync -av --exclude '.DS*'  $masterdetail_arch_ios_dir"/" $app_ios_dir
     #Copy local Android directories to app's path
     echo "Coppying Android spesific directories to the app path..."
     rsync -av --exclude '.DS*' --exclude 'AndroidManifest.xml' $masterdetail_arch_android_dir"/" $app_android_dir
     #Copy also the manifest in the correct app directory
     cp -f $masterdetail_arch_android_dir"/AndroidManifest.xml" $app_android_dir"/Properties/"
   fi

   #remove useless files from app_ios_dir
   find "$app_basename_dir_path" -maxdepth 1 -name "AssemblyInfo.*" -type f -exec rm '{}' \;
   find "$app_basename_dir_path" -maxdepth 1 -name "MainPage.*" -type f -exec rm '{}' \;


   #Change ownership so that it much the users one
   sudo chown -R $(whoami) "$abs_path_dir/"
   #Install nuget packages and build proj
   ./python_scripts/nuget_packages.py $app_csproj_file

   #Build global .sln
   # echo "Building global sln..."
   # dotnet build $abs_path_dir"/$app_basename.sln"
#   rm -f .git/index
#   git reset
#   git add -A
#   git reset --hard HEAD

   cd ../
   rm -rf ./xamarin-forms-app-initialization/
   git clone https://gitlab.com/petran199/xamarin-forms-app-initialization.git

else
   echo "Error: $abs_path_dir not found"
fi
