# Define your app path in the variable below

secrets={

    "LATEST_APP_NAME":"MyApp",
    "NUGET_PACKAGES":[
                      "Microsoft.CSharp --version 4.7.0",
                      "Autofac --version 4.9.4",
                      "Xam.Plugin.Connectivity --version 3.2.0",
                      "Refractored.MvvmHelpers --version 1.3.0",
                      "Newtonsoft.Json --version 12.0.3",
                      "Plugin.Permissions --version 3.0.0.12",
                      "Xam.Plugins.Settings --version 4.1.0-beta",
                      "Acr.UserDialogs --version 7.0.35",
                      "Plugin.CrossPlatformTintedImage --version 1.0.0",
                      "Rg.Plugins.Popup --version 1.2.0.223",
                      "Xamarin.Forms --version 4.4.0.991265",
                      "Xamarin.Essentials --version 1.3.1"
                      ]
}
