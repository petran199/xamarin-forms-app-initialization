#!/usr/bin/env python

import constants
import search_and_replace, copy_dirs_to_path
import os, sys
from shutil import  ignore_patterns


script_basename = os.path.basename(sys.argv[0])
mbl_app_basename = os.path.basename(sys.argv[4])
tabbed_script_info_plist = "%s/tabbed_architecture_setup/iOS/Info.plist" % os.getcwd()
mbl_info_plist = "%s/%s.iOS/Info.plist" % (sys.argv[1],mbl_app_basename)
script_usage = "usage: %s absolute_app_dir\
       target_app_cf_bundle_identifier\
       cf_bundle_identifier_to_be_changedcop\
       mbl_app_basename" % script_basename
app_proj_path =  "%s/%s/%s.csproj" % \
                  (sys.argv[1], mbl_app_basename,mbl_app_basename)


def update_app_name_in_files(search_text,replace_text):
    files_path = []

    # r=root, d=directories, f=files
    for r, d, f in os.walk(os.getcwd()):
        for file in f:
            if '.' not in file[0]:
                files_path.append(os.path.join(r, file))

    for file_path in files_path:
        search_and_replace.replace_text(search_text,replace_text,file_path)


def check_args_before_update_app_name_in_files():

    latest_app_name = constants.secrets["LATEST_APP_NAME"]

    if  len(sys.argv) != 6:
        print script_usage
        sys.exit()
    else:
        #No reason to make any changes
        if mbl_app_basename == latest_app_name:
           print """Skip updating the name in folder, the app name is the same
           with the one in local folders..."""
           return
        print "Updating the app name in folders..."
        update_app_name_in_files(latest_app_name, mbl_app_basename)
        constants.secrets["LATEST_APP_NAME"] = mbl_app_basename

def update_cf_bundle_identifier():
        print "Updating the CFBundeIdentifier..."
        search_and_replace.replace_text(sys.argv[2], sys.argv[3],
                                         sys.argv[5])

def main():
    check_args_before_update_app_name_in_files()
    update_cf_bundle_identifier()
    # copy_dirs_to_path.copytree(os.getcwd(), app_abolute_path)
    # nuget_packages.install(app_proj_path)



if __name__== "__main__":
    main()
