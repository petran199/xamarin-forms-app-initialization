#!/usr/bin/env python

import shutil,os

def copytree(src, dst, symlinks=False,
             ignore=shutil.ignore_patterns('*.pyc', '*.py', '*.sh','.DS*')):
    print "Coppying directories in mobile app direcory..."
    for item in os.listdir(src):

        if os.path.isdir(item) != True:
           continue

        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.exists(d):
            try:
                shutil.rmtree(d)
            except Exception as e:
                print e
                os.unlink(d)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)
