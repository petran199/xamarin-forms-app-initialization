#!/usr/bin/env python

import sys,os

def replace_text(search_text,replace_text,input_file):

    input = open(input_file,'r')
    stext = search_text
    rtext = replace_text
    output = ""

    for s in input.xreadlines():
        output += s.replace(stext, rtext)

    input.close()
    input = open(input_file,'w')
    input.write(output)
    input.close()
