#!/usr/bin/env python

import constants, os, subprocess, sys

#Vars
# app_abs_path = constants.secrets["APP_ABSOLUTE_PATH"]
script_usage =  "usage: %s app_csproj_path"% sys.argv[0]
nuget_packages = constants.secrets["NUGET_PACKAGES"]
app_proj_path = sys.argv[1]
# app_proj_path =  "%s/%s.csproj" % \
#                   (app_abs_path, os.path.basename(app_abs_path))

# def install(app_proj_path):
if  len(sys.argv) != 2:
    print script_usage
    sys.exit()
else:
    print "Installing required nuget packages..."
    for nuget_package in nuget_packages:
        nuget_pkg = nuget_package.split(" ")
        subprocess.call(["dotnet","add", app_proj_path,"package",
                        nuget_pkg[0],nuget_pkg[1],nuget_pkg[2]])

#now build the project
subprocess.call(["dotnet", "build", app_proj_path])
