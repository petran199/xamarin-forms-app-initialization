﻿using Xamarin.Forms;

namespace MyApp.SubViews
{
    public partial class SubViewLoading : ContentView
    {
        public string LoadingText
        {
            get
            {
                return labelLoading.Text;
            }
            set
            {
                labelLoading.Text = value;
                labelLoading.IsVisible = !string.IsNullOrEmpty(value);
            }
        }

        public SubViewLoading()
        {
            InitializeComponent();
        }
    }
}
