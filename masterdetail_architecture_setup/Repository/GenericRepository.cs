﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MyApp.Contracts.Repository;
using MyApp.Helpers;

namespace MyApp.Repository
{
    public class GenericRepository : IGenericRepository
    {
        private HttpClient CreateHttpClient(string authToken)
        {
            var httpClient = new HttpClient { BaseAddress = new Uri(Constants.BASE_URL) };

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);

            return httpClient;
        }

        private async Task<string> GetRequestAsync(HttpClient httpClient, string url)
        {
            // make the request
            HttpResponseMessage response = await httpClient.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();   
                }
                else
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }

            // parse the response and return the data
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> AuthenticateAsync(string uri, string username, string password)
        {
            using (var client = new HttpClient())
            {
                // We want the response to be JSON
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                // Build up the data to POST
                var postData = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("username", username),
                    new KeyValuePair<string, string>("password", WebUtility.UrlEncode(password)),
                    new KeyValuePair<string, string>("grant_type", "password")
                };

                var content = new FormUrlEncodedContent(postData);

                // Post to the Server and parse the response
                HttpResponseMessage response = await client.PostAsync($"{Constants.BASE_URL}{uri}", content);
                var jsonString = await response.Content.ReadAsStringAsync();
                object responseData = JsonConvert.DeserializeObject(jsonString);

                // return the Access Token
                return ((dynamic)responseData).access_token;
            }
        }

        public async Task<T> GetAsync<T>(string uri, string authToken)
        {
            var httpClient = CreateHttpClient(authToken);
            var jsonString = await GetRequestAsync(httpClient, uri);

            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public async Task<T> PostAsync<T>(string uri, T data, string authToken)
        {
            HttpClient httpClient = CreateHttpClient(authToken);

            var content = JsonConvert.SerializeObject(data, new JsonSerializerSettings { ContractResolver = new JsonPropertiesIngoreSerializationResolver() });
            var response = await httpClient.PostAsync(uri, new StringContent(content, Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public async Task<TR> PostAsync<T, TR>(string uri, T data, string authToken)
        {
            HttpClient httpClient = CreateHttpClient(authToken);

            var content = JsonConvert.SerializeObject(data, new JsonSerializerSettings { ContractResolver = new JsonPropertiesIngoreSerializationResolver() });
            var response = await httpClient.PostAsync(uri, new StringContent(content, Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<TR>(await response.Content.ReadAsStringAsync());
        }

        public async Task DeleteAsync(string uri, string authToken)
        {
            HttpClient httpClient = CreateHttpClient(authToken);
            await httpClient.DeleteAsync($"{Constants.BASE_URL}{uri}");
        }
    }
}
