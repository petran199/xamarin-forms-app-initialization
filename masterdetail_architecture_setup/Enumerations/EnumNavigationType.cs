﻿namespace MyApp.Enumerations
{
    public enum EnumNavigationType
    {
        MainPage,
        DetailPage,
        SimpleNavigationPage,
        ModalPage,
        PopupPage
    }
}
