﻿using System;

namespace MyApp.Exceptions
{
    public class NoInternetConnectionException : Exception
    {
        public NoInternetConnectionException()
                : base("Error No internet connection")
        {
        }
    }
}
