﻿using System;
using System.Net;
using System.Net.Http;

namespace MyApp.Exceptions
{
    public class HttpRequestExceptions : HttpRequestException
    {
        public HttpStatusCode HttpCode { get;}

        public HttpRequestExceptions(HttpStatusCode code) : this(code, null, null)
        {
            HttpCode = code;
        }

        public HttpRequestExceptions(HttpStatusCode code, string message) : this(code, message, null)
        {
            HttpCode = code;
        }

        public HttpRequestExceptions(HttpStatusCode code, string message, Exception inner) : base(message, inner)
        {
            HttpCode = code;
        }
    }
}
