﻿namespace MyApp.Helpers
{
    public static class Constants
    {
        //General
        public const string DOWNLOAD_APP_URL = "DOWNLOAD_APP_URL";
        public const string PUBLIC_FOLDER = "PUBLIC_FOLDER";
        public const string BASE_URL = "BASE_URL";

        //APIs

        //User
        public const string REST_API_GET_USER = "REST_API_GET_USER";

        //Misc
        public const string REST_API_GET_LATEST_RELEASE_VERSION = "REST_API_GET_LATEST_RELEASE_VERSION";

        //SendEmail - IEmailDataService
        public static readonly string MAIL_RECEIVER_SUPPORT = "MAIL_RECEIVER_SUPPORT";
        public const string MAIL_SUBJECT_CONTACT_WITH_DEV_TEAM = "MAIL_SUBJECT_CONTACT_WITH_DEV_TEAM";
        public const string MAIL_SUBJECT_SHARE_APP = "MAIL_SUBJECT_SHARE_APP";
        public const string REST_API_REPORT_THE_ERROR = "api/ReportAnError";

        //GDPR
        public const string PRIVACY_POLICY_URL = "PRIVACY_POLICY_URL";
        public const string TERMS_OF_USE_URL = "TERMS_OF_USE_URL";

        //Appcenter push notification
        public const string APP_CENTER_ANDROID_SECRET = "APP_CENTER_ANDROID_SECRET";
        public const string APP_CENTER_IOS_SECRET = "APP_CENTER_IOS_SECRET";
    }
}
