﻿using System.Threading.Tasks;
using MyApp.Contracts.Services.General;

namespace MyApp.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {

        public HomeViewModel(IConnectionService connectionService,
                             INavigationService navigationService,
                             IDialogService dialogService)

            : base(connectionService, navigationService, dialogService)
        {

        }

        public override async Task InitializeAsync(object data)
        {
            await _connectionService.CheckIfUpdateIsAvailable();
        }

    }
}
