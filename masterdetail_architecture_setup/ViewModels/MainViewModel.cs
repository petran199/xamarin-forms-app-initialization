using System.Threading.Tasks;
using MyApp.Contracts.Services.General;

namespace MyApp.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private ISettingsService _settingsService;
        private MenuViewModel _menuViewModel;

        public MenuViewModel MenuViewModel
        {
            get => _menuViewModel;
            set
            {
                _menuViewModel = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel(IConnectionService connectionService,
                             INavigationService navigationService,
                             IDialogService dialogService,
                             ISettingsService settingsService,
                             MenuViewModel menuViewModel) :
                             base(connectionService, navigationService, dialogService)
        {
            _menuViewModel = menuViewModel;
            _settingsService = settingsService;
        }

        public override async Task InitializeAsync(object data)
        {
            await _menuViewModel.InitializeAsync(data);
        }
    }
}
