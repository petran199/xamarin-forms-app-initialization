﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using MyApp.Enumerations;
using MyApp.Contracts.Services.General;
using MyApp.Models;
using MyApp.ResX;
using Xamarin.Forms;

namespace MyApp.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {

        private ObservableCollection<MainMenuItem> _menuItems;
        private readonly ISettingsService _settingsService;

        public string WelcomeText => "Hello " + _settingsService.LastLoggedUsername;

        public ICommand MenuItemTappedCommand => new Command(OnMenuItemTapped);
        //public ICommand ProfileTappedCommand => new Command(OnProfileTapped);

        public ObservableCollection<MainMenuItem> MenuItems
        {
            get => _menuItems;
            set
            {
                _menuItems = value;
                OnPropertyChanged();
            }
        }

        public MenuViewModel(IConnectionService connectionService,
                             INavigationService navigationService,
                             IDialogService dialogService,
                             ISettingsService settingsService)
                             :base(connectionService,
                                   navigationService,
                                   dialogService)
        {
            _settingsService = settingsService;
            MenuItems = new ObservableCollection<MainMenuItem>();
            LoadMenuItems();
        }

        public override async Task InitializeAsync(object data)
        {
        }

        private void OnMenuItemTapped(object menuItemTappedEventArgs)
        {
            var menuItem = ((menuItemTappedEventArgs as ItemTappedEventArgs)?.Item as MainMenuItem);

            if (menuItem != null && menuItem.MenuItemType == MenuItemType.Logout)
            {
                _settingsService.LastLoggedId = 0;
                _settingsService.LastLoggedUsername = null;
                _settingsService.IsUserLoggedIn = false;
                _settingsService.AuthToken = string.Empty;
                _navigationService.ClearBackStack();
            }

            var type = menuItem?.ViewModelToLoad;
            _navigationService.NavigateToAsync(type);
        }

        private void LoadMenuItems()
        {
            MenuItems.Add(new MainMenuItem
            {
                MenuText = "Home",
                ViewModelToLoad = typeof(HomeViewModel),
                MenuItemType = MenuItemType.Home
            });

            MenuItems.Add(new MainMenuItem
            {
                MenuText = "Logout",
                ViewModelToLoad = typeof(LoginViewModel),
                MenuItemType = MenuItemType.Logout
            });
        }

    }
}
