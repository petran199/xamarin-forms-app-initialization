﻿namespace MyApp.Models
{
    public class AuthenticationRequest
    {
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
