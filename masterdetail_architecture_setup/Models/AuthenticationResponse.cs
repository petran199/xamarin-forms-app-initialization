﻿namespace MyApp.Models
{
    public class AuthenticationResponse
    {
        public bool IsAuthenticated { get; set; }
        public UserModel User { get; set; }
    }
}
