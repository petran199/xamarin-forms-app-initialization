﻿using System.Threading.Tasks;

namespace MyApp.Contracts.Services.General
{
    public interface IDialogService
    {
        Task ShowDialog(string message, string title, string buttonLabel);
        Task<string> ActionSheetAsync(string title, string cancel, string destructive, params string[] buttons);
        Task<bool> ConfirmAsync(string message, string title, string okText, string cancelText);
        void ShowToast(string message);
    }
}
