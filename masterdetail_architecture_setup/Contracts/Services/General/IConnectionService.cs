﻿using System.Threading.Tasks;
using Plugin.Connectivity.Abstractions;

namespace MyApp.Contracts.Services.General
{
    public interface IConnectionService
    {
        event ConnectivityChangedEventHandler ConnectivityChanged;

        Task<bool> DoIHaveInternet(bool silentCheck = false);

        Task<bool> CheckIfUpdateIsAvailable();
    }
}
