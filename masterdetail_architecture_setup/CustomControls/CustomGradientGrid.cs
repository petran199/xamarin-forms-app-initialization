﻿using Xamarin.Forms;

namespace MyApp.CustomControls
{
    public class CustomGradientGrid : Grid
    {
        public Color StartColor { get; set; }
        public Color EndColor { get; set; }
    }
}
