﻿using MyApp.CustomControls;
using MyApp.iOS.Renderers;
using CoreAnimation;
using CoreGraphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomGradientGrid), typeof(CustomGradientGridRenderer))]
namespace MyApp.iOS.Renderers
{
    public class CustomGradientGridRenderer : VisualElementRenderer<Grid>
    {
        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            CustomGradientGrid grid = (CustomGradientGrid)Element;
            CGColor startColor = grid.StartColor.ToCGColor();
            CGColor endColor = grid.EndColor.ToCGColor();

            var gradientLayer = new CAGradientLayer()
            {
              StartPoint = new CGPoint(1,0),
              EndPoint = new CGPoint(0,1)
            };

            gradientLayer.Frame = rect;
            gradientLayer.Colors = new CGColor[] { startColor, endColor };

            NativeView.Layer.InsertSublayer(gradientLayer, 0);
        }
    }
}
