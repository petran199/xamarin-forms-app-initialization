using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyApp.Enumerations;
using MyApp.Contracts.Services.General;
using MyApp.ViewModels;
using MyApp.Views;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace MyApp.Services.General
{

    public class NavigationService : INavigationService
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingsService _settingsService;
        private readonly Dictionary<Type, Type> _mappings;

        private Application CurrentApplication => Application.Current;

        public NavigationService(IAuthenticationService authenticationService,
                                 ISettingsService settingsService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingsService;
            _mappings = new Dictionary<Type, Type>();

            CreatePageViewModelMappings();
        }

        public async Task InitializeAsync()
        {
            if (_authenticationService.NeedsToLogin())
            {
                await NavigateToAsync<LoginViewModel>();
            }
            else
            {
                await NavigateToAsync<MainViewModel>();
            }
        }

        public async Task NavigateBackAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.Detail.Navigation.PopAsync();
            }
            else if (CurrentApplication.MainPage != null)
            {
                await CurrentApplication.MainPage.Navigation.PopAsync();
            }
        }

        public async Task ClearBackStack()
        {
            await CurrentApplication.MainPage.Navigation.PopToRootAsync();
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        public Task NavigateToAsync(Type viewModelType)
        {
            return InternalNavigateToAsync(viewModelType, null);
        }

        public Task NavigateToAsync(Type viewModelType, object parameter)
        {
            return InternalNavigateToAsync(viewModelType, parameter);
        }

        public async Task PopToRootAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.Detail.Navigation.PopToRootAsync();
            }
        }

        public async Task PopBackAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.Detail.Navigation.PopModalAsync();
            }
        }

        public async Task PopPopupAsync()
        {
            await PopupNavigation.Instance.PopAsync();
        }

        protected virtual async Task InternalNavigateToAsync(Type viewModelType, object parameter)
        {
            var page = CreatePage(viewModelType, parameter);
            var enumNavigationType = GetNavigationType(page);

            await NavigateToProperType(enumNavigationType, page);

            await (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
        }

        private Page CreatePage(Type viewModelType, object parameter)
        {
            try
            {
                Type pageType = GetPageTypeForViewModel(viewModelType);

                if (pageType == null)
                {
                    throw new Exception($"Mapping type for {viewModelType} is not a page");
                }

                var page = Activator.CreateInstance(pageType) as Page;

                return page;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            if (!_mappings.ContainsKey(viewModelType))
            {
                throw new KeyNotFoundException($"No map for ${viewModelType} was found on navigation mappings");
            }

            return _mappings[viewModelType];
        }

        private EnumNavigationType GetNavigationType(Page page)
        {
            switch (page)
            {
                case MainView _:
                case LoginView _:
                    return EnumNavigationType.MainPage;

                case HomeView _:
                    return EnumNavigationType.DetailPage;

                default:
                    return EnumNavigationType.SimpleNavigationPage;
            }
        }

        private async Task NavigateToProperType(EnumNavigationType navigationType, Page page)
        {
            var currentPage = CurrentApplication.MainPage as MainView;
            if (navigationType == EnumNavigationType.MainPage)
            {
                CurrentApplication.MainPage = page;
            }
            else if (navigationType == EnumNavigationType.PopupPage)
            {
                await CurrentApplication.MainPage.Navigation.PushPopupAsync((PopupPage)page);
            }
            else if (navigationType == EnumNavigationType.ModalPage)
            {
                await currentPage.Detail.Navigation.PushModalAsync(page);
                currentPage.IsPresented = false;
            }
            else if (navigationType == EnumNavigationType.DetailPage)
            {
                currentPage.Detail = new NavigationPage(page);
                currentPage.IsPresented = false;
            }
            else
            {
                await currentPage.Detail.Navigation.PushAsync(page);
                currentPage.IsPresented = false;
            }
        }

        private void CreatePageViewModelMappings()
        {
            _mappings.Add(typeof(LoginViewModel), typeof(LoginView));
            _mappings.Add(typeof(MainViewModel), typeof(MainView));
            _mappings.Add(typeof(MenuViewModel), typeof(MenuView));
            _mappings.Add(typeof(HomeViewModel), typeof(HomeView));
        }
    }
}
