﻿using System;
using System.Threading.Tasks;
using MyApp.Contracts.Repository;
using MyApp.Contracts.Services.General;
using MyApp.Helpers;
using MyApp.Models;

namespace MyApp.Services.General
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IGenericRepository _genericRepository;
        private readonly ISettingsService _settingsService;
        public AuthenticationService(IGenericRepository genericRepository,
                                     ISettingsService settingsService)
        {
            _settingsService = settingsService;
            _genericRepository = genericRepository;

        }

        public bool NeedsToLogin()
        {
            if (_settingsService.LastLogin.Date <= DateTime.Now.Date.AddDays(-1))
            {
                return true;
            }

            var needsToLogin = !_settingsService.IsUserLoggedIn;

            if (!needsToLogin)
            {
                _settingsService.LastAppOpened = DateTime.Now.AddHours(1);
            }

            return needsToLogin;
        }

        public async Task<AuthenticationResponse> Authenticate(string username, string password)
        {
            var token = await _genericRepository.AuthenticateAsync("token", username, password);

            var authResponse = new AuthenticationResponse();

            if (string.IsNullOrEmpty(token))
            {
                authResponse.IsAuthenticated = false;
            }
            else
            {
                _settingsService.AuthToken = token;
                authResponse.IsAuthenticated = true;

                authResponse.User = await _genericRepository.GetAsync<UserModel>($"{Constants.REST_API_GET_USER}?username={username}", _settingsService.AuthToken);
            }

            return authResponse;
        }


    }
}
